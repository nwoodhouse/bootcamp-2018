<?php

// base / parent class to describe Alacritites
class Human
{
	// properties that will be inherited by all types of Human
	private $age;
	private $name;
	private $isMale;

	public function __construct($name, $age, $isMale)
	{
		$this->age = $age;
		$this->name = $name;
		$this->isMale = $isMale;
	}

	public function getInfoString()
	{
		return "$this->name is $this->age and they are " . ( $this->isMale ? 'male' : 'female' );
	}
}

class Male extends Human
{
	public function __construct($name, $age)
	{
		// since all instances of this class are instances of
		// the Human class also, and also since the Human
		// constructor requires a name, age and isMale property,
		// we must instruct the parent class with how it should
		// be built.
		parent::__construct($name, $age, true);
	}

	// note that this method is specific to intances of this
	// class - and also that this method makes use of a method
	// that we inherit from our parent class
	public function getGreeting()
	{
		return 'Yoo, what up dog?! ' . $this->getInfoString();
	}
}

// This class will inherit all the properties and behaviour of
// the base (aka parent) class!
class Female extends Human
{
	public function __construct($name, $age)
	{
		parent::__construct($name, $age, false);
	}
}

// build a few Humans
$p1 = new Male('Nathan', 38);
$p2 = new Female('Mandi', 23);

// note that we can invoke methods available from the base class!
echo $p2->getGreeting();

// we can also invoke the methods that are only available in the
// child classes!
echo $p1->getGreeting();
