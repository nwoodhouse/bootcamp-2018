<html>
<body>

	<?php
		
		class Person
		{
			private $age;
			private $name;

			// constructor function - called
			// whenever someone builds a new
			// Person instance, using 'new'
			public function __construct($name, $age)
			{
				$this->name = $name;
				$this->age = $age;
			}

			public function getAge()
			{
				return $this->age;
			}

			public function getName()
			{
				return $this->name;
			}
		}

		class Player
		{
			public $personDetails;
			public $position;

			public function __construct($name, $age, $position)
			{
				$this->personDetails = new Person($name, $age);
				$this->position = $position;
			}
		}


		$davidBeckham = new Player('Dave', 50, 'Goalie');
	?>

	<?= $davidBeckham->personDetails->getName() ?>
	
</body>
</html>