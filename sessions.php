<?php
	// ensure we can access the HTTP session
	session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<?php		

		// retrieve var from session - default to 0 if
		// it hasn't previously been set
		$count = array_key_exists('count', $_SESSION)
			? $_SESSION['count']
			: 0;
		
		// modify it & restore in the session
		$count++;
		$_SESSION['count'] = $count;

	?>

	<h1>You have viewed this page: <?= $count ?> times</h1>

</body>
</html>