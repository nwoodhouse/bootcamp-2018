<?php

// This class provides a way of creating a bag of numbers, 
// bounded by a give range, that satisfy a given filter
// criteria. Note that this class also provides an abstraction
// of the condition under which a number will be added to the
// bag - this is given by the 'filter' - a callable function
// that would return 
class GenericBag
{
	private $min;
	private $max;
	private $filter;
	private $bag;

	public function __construct($min, $max, $filter)
	{
		$this->min = $min;
		$this->max = $max;
		$this->filter = $filter;
		$this->bag = null;
	}

	public function getBag()
	{
		if (!$this->bag)
			$this->bag = $this->buildBag();

		return $this->bag;
	}

	public function sumBag()
	{
		return array_sum($this->getBag());
	}

	private function buildBag()
	{
		$bag = [];

		for($i=$this->min; $i<$this->max; $i++)
		{
			if(($this->filter)($i))
			{
				$bag[] = $i;
			}
		}

		return $bag;
	}
}

$bagOfNumbersDivisibleBy123 = new GenericBag(100, 399, function ($i) {
	return $i % 123 === 0;
});

$oddBag = new GenericBag(0, 100, function ($i) {
	return $i % 2;
});

echo $oddBag->sumBag();




