<html>
<body>

	<?php
		
		// take a board size
		// return an array detailing the number
		// of grains of rice per square
		function buildBoard($size)
		{
			$board = [];

			for ($index = 0, $grains = 1; $index < $size; $index++, $grains *= 2)
			{
				$board[] = $grains;
			}

			return $board;
		}

		$board = buildBoard(30);


	?>
	
	<h1>
		Grains on a chess board
	</h1>

	<?php
		include('https://www.bbc.co.uk/sport/football/45405476');

		// iterate through each square in the board
		foreach ($board as $i => $square) {
			require('squareInfo.php');
		}
	?>
</body>
</html>