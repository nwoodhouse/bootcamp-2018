<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/game');
        $response->assertOk();
        $response->assertViewHas('userName', 'Nathan');
        $response->assertSee('Go!!');


        $response->assertStatus(200);
    }

}
