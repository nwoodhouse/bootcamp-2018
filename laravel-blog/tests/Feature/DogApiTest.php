<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Tests the correctness of the Dogs api (See DogController)
 *
 * Class DogApiTest
 * @package Tests\Feature
 */
class DogApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
    	// test as an unauthenticated user
        $unauthResponse = $this->get( route('dogs.index') );
        $unauthResponse->assertRedirect();

        $this
			->actingAs(User::first())
			->get(route('dogs.index'))
        	->assertSee('Bo')
			->assertJsonStructure([
				[
					'id',
					'name'
				]
			]);


    }
}
