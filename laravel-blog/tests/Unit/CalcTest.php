<?php

namespace Tests\Unit;

use App\Http\Controllers\CalcController;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CalcTest extends TestCase
{
	private $calc;

	protected function setUp()
	{
		parent::setUp();
		echo 'here!';
	}

	protected function tearDown()
	{
		parent::tearDown();
		echo 'dying!!';
	}


	/**
     * A basic test example.
     *
     * @return void
     */
    public function testAddition()
    {
    	$calculator = new CalcController();

		$x = 1;
		$y = 2;
		$this->assertEquals(3, $calculator->add($x, $y), 'Simple addition of 2 positives');

		$this->assertEquals(5, $calculator->add(2, 3), 'Simple addition of 2 positives');

		$this->assertEquals(0, $calculator->add(1, -1), 'Pos and neg');

		$this->assertEquals(-10, $calculator->add(-7, -3), 'Neg and neg');

		$this->assertEquals(0, $calculator->add(0, 0), 'Zero');

    }

    public function testSomethingElse()
	{
		$this->assertTrue(true);
	}

	public function testSomethingElse2()
	{
		$this->assertTrue(true);
	}

}
