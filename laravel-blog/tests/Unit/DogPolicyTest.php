<?php

namespace Tests\Unit;

use App\Dog;
use App\Policies\DogPolicy;
use App\User;
use phpDocumentor\Reflection\Types\Integer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DogPolicyTest extends TestCase
{
	/** @var User */
	private $owner;

	/** @var Dog */
	private $dog;

	protected function setUp()
	{
		parent::setUp();
		$this->owner = new User();
		$this->owner->id = random_int(1, 1000);

		$this->dog = new Dog([
			'owner_id' => $this->owner->id
		]);
	}

	/**
	 * Make sure that the starting conditions for the test are
	 * as we expect!
	 */
	public function testSetup()
	{
		$this->assertNotNull($this->owner, 'Should be able to create a user!');
		$this->assertEquals($this->dog->owner_id, $this->owner->id, 'Dog belongs to owner');
		$this->assertTrue($this->dog->ownedBy($this->owner), 'Dog belongs to owner');

	}

	/**
     * A basic test example.
     *
     * @return void
     */
    public function testCrudMethods()
	{
    	$this->assertTrue($this->owner->can('view', $this->dog), 'Owner can view the dog');

    	// create a non-owner User
		$nonOwner = new User();
		$nonOwner->id = $this->owner->id + 1;

		$this->assertTrue($nonOwner->cannot('view', $this->dog), 'Non-owner cannot view');
    }
}
