<?php

namespace Tests\Unit;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
	use RefreshDatabase;

	protected function setUp()
	{
		echo 'Preping...';
	}

	protected function tearDown()
	{
		echo 'Done!';
	}

	/**
     * A basic test example.
     *
     * @return void
     */
    public function testMultiplication()
    {
        $post = new Post();

        // test pos * pos
        $this->assertEquals(6, $post->multiply(2, 3));

        // test pos * neg
		$this->assertGreaterThan($post->multiply(1, -1), 0);

		// test 0 * ?

		// test very large
    }
}
