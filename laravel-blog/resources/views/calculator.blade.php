@extends('layouts.master')

@section('title')
	Calculator results for {{ $x }} and {{ $y }}!
@endsection

@section('content')
	<h2>
		The result: {{ $result }}
	</h2>
@endsection