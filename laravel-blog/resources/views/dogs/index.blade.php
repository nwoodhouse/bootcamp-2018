@extends('layouts.master')

@section('content')
    <div id="dogs-widget">
        <h1>Dogs! @{{ dogs.length }} </h1>

        <ul class="list-group">
            <a v-for="dog in dogs" @click.prevent="setActive(dog)" href="#" class="list-group-item list-group-item-action">
                @{{ dog.name }}
            </a>
        </ul>

        <br>
        <div v-if="activeDog" class="card" :class="{deleting: activeDog.deleting}" style="width: 18rem;">
            <img class="card-img-top" :src="activeDog.img" :title="getImageDesc()">
            <div class="card-body">
                <h5 class="card-title" v-text="activeDog.name">@{{ activeDog.name }}</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a :href="activeDog.url" target="_blank" class="btn btn-primary">More info</a>

                <a @click.prevent="destroyDog(activeDog)" :disabled="activeDog.deleting" href="#" class="float-right text-danger btn btn-link">
                    Delete
                </a>
            </div>
        </div>
        <div v-else>
            <h3>
                Click on a dog to select them!
            </h3>
        </div>

    </div>

@endsection


@section('js')
    @parent

    <script>

        var app = new Vue({
            el: '#dogs-widget',

            // state
            data: {
                dogs: [],
                activeDog: null
            },

            computed: {

            },

            // operations
            methods: {

                destroyDog: function (dog) {
                    Swal({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Delete'
                    }).then((result) => {
                        if (result.value) {
                            dog.deleting = true;
                            var url = this.getApiUrl(dog);

                            axios.delete(url).then((response) => {

                                // we know that the dog has been deleted on the
                                // server - delete it's cache in the client
                                var index = this.dogs.indexOf(dog);
                                if (index != -1) {
                                    this.dogs.splice(index, 1);

                                    if (this.activeDog == dog)
                                        this.activeDog = null;
                                }
                            }).catch(() => {
                                console.log('error');
                            }).then(() => {
                                dog.deleting = false;
                            });

                            app.$forceUpdate();
                        }
                    });
                },

                getImageDesc: function () {
                    return 'Image of: ' + this.activeDog.name;
                },

                // set a given dog as the active dog
                setActive: function (dog) {
                    this.activeDog = dog;
                    this.getDog(this.activeDog);
                },

                // Go off to the dogs API and retrieve all dogs
                getDogs: function () {

                    axios.get(window.routes.api.dogs.index)
                         .then(response => this.dogs = response.data)
                         .catch(response => alert('Problem fetching dogs!'))
                },

                // go off to the DB and retrieve the info for the given dog
                getDog: function (dog) {
                    var url = this.getApiUrl(dog);
                    axios.get(url)
                         .then((response) => {
                             dog.img = response.data.img;
                             dog.url = response.data.url;

                             // kick the view template into re-rendering
                             this.$forceUpdate();
                         });
                },

                getApiUrl: function (dog) {
                    var urlString = window.routes.api.dogs.show;
                    return window.routes.buildUrl(urlString, dog);
                }
            },
            mounted: function () {
                this.getDogs();
            }

        });
    </script>

@endsection