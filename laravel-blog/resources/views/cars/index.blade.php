@extends('layouts.master')

@section('content')

	<style>
		[v-cloak] {
			display: none;
		}
	</style>
	
	<div class="showroom" v-cloak>
		<h1 >
			@{{ title }}
		</h1>
		<hr/>
		
		<div class="row">
			<div class="col-md-9" v-if="featuredCar">
				
				<h2>@{{ featuredCar.title }}</h2>

				<img src="https://images.lexus-europe.com//gb/CT/CT%20200h/F%20SPORT/width/740/height/340/scale-mode/0/padding/0,0/day-exterior-06_8X1.png" alt="">

			</div>

			<div class="col-md-3">
				<div v-for="car in cars">
					<button class="btn btn-primary btn-lg" @click="setFeatured(car)">
						@{{ car.title }}
					</button>
					<br>
					<br>
				</div>

				<button class="btn btn-block btn-success" @click="showAddCarModal">Add</button>
			</div>
		</div>

		{{-- New Car modal --}}
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <form>
		          <div class="form-group">
		            <label class="col-form-label">Title:</label>
		            <input v-model="newCar.title" type="text" class="form-control">
		          </div>
		          <div class="form-group">
		            <label class="col-form-label">Description:</label>
		            <textarea v-model="newCar.description" class="form-control"></textarea>
		          </div>
		        </form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary" @click="addCar(newCar)">Add car</button>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
	

@endsection

@section('js')
	<script>

		var cars = {!! $cars !!};
		var storeCarUrl = '{{ route('cars.store') }}';

		// set things up so that Vue JS drives the 'showroom' div
		var app = new Vue({
			el: '.showroom',
			data: {
				title: 'Welcome to the showroom!',
				cars: cars,
				featuredCar: null,
				newCar: {
					title: '',
					description: ''
				},
				urls: {
					storeCar: storeCarUrl
				}
			},
			methods: {
				setFeatured: function (car) {
					this.featuredCar = car;
				},

				// causes the new car modal to be displayed
				showAddCarModal: function() {

					// reset the new car so that the form doesn't keep old data
					this.newCar = {};

					$('#exampleModal').modal('show');
				},

				hideCarModal: function () {
					$('#exampleModal').modal('hide');
				},

				// cause the car model to be written
				addCar: function (newCar) {
					var car = {
						title: newCar.title,
						description: newCar.description
					};
					this.cars.push(car);

					this.hideCarModal();

					this.setFeatured(car);

					// send the new car details up to the server to store in the DB
					this.storeCar(car);
				},

				// AJAX car details up to the server
				storeCar: function (car) {

					car._token = '{{ csrf_token() }}';

					$.post(this.urls.storeCar, car, function () {
						
					}).fail(function () {
						alert('failed!');
					});
				}
			}
		});
		

	</script>
@endsection











