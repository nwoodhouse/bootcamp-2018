@extends('layouts.master')

@section('content')

	<h1>All posts!</h1>

	{{ $posts->links() }}

	<ul class="list-group">
		@foreach($posts as $post)
			<li class="list-group-item">
				{{ $post->title }}
				<span class="float-right badge badge-primary">
					by: 
					{{ $post->user->name }}
				</span>

				<form class="delete-form" method="post" action="{{ route('posts.destroy', $post->id) }}">
					@method('DELETE')
					@csrf
					<button class="float-right delete btn btn-danger btn-lg">
						Delete
					</button>
				</form>
			</li>
		@endforeach
	</ul>

	<script>
		var forms = document.getElementsByClassName('delete-form');

		var handler = function (event) {
			var doDelete = confirm('Are you sure you want to delete?');
			if (!doDelete)
				event.preventDefault();
		};

		for(var i=0; i<forms.length; i++)
		{
			var form = forms[i];
			form.onclick = handler;
		}
	</script>

@endsection







