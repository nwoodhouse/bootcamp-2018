@extends('layouts.master')

@section('content')

	<h1>All Users</h1>

	<div class="row">
		@foreach ($users as $user)
			<div class="col-md-6">
				@include('users.user-info', compact('user'))
			</div>
		@endforeach
	</div>

@endsection