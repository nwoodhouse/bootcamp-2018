<div class="card user-info">
	<img src="https://picsum.photos/200/100/?random" class="card-img-top">
	<div class="card-body">
	    <h5 class="card-title">{{ $user->name }}</h5>
	    <p class="card-text">{{ $user->email }}</p>
	    <a href="{{ $user->url }}" class="btn btn-primary btn-block">Info</a>
  	</div>
</div>