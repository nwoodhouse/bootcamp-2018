
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Static Top Navbar Example for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
      .wrapper {
        height: 600px; 
        width: 700px; 
        background-image:url('/misunderstanding.png');
        background-size: 100% 100%;
      }

      .block {
        height: 100%;
      }

      .dark {
        background-color: red;
        opacity: 1;
      }

      .row {
        height: 200px;
      }
    </style>
  </head>

  <body>

    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Default</a></li>
            <li class="active"><a href="./">Static top <span class="sr-only">(current)</span></a></li>
            <li><a href="../navbar-fixed-top/">Fixed top</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container">

		<div class="wrapper">
			<div class="row">
        <div class="col-xs-6 dark block">
        </div>
        <div class="col-xs-6 dark block">
        </div>
      </div>
      
      <div class="row">
        <div class="col-xs-6 dark block">
        </div>
        <div class="col-xs-6 dark block">
        </div>
      </div>

      <div class="row">
        <div class="col-xs-6 dark block">
        </div>
        <div class="col-xs-6 dark block">
        </div>
      </div>

      <br>
      <button class="go btn btn-primary btn-lg btn-block">
        Go!!
      </button>

      <button class="reset btn btn-default btn-lg btn-block">
        Next...
      </button>

    </div> <!-- /container -->
    
		</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://getbootstrap.com/docs/3.3/dist/js/bootstrap.min.js"></script>
    <script>

      var $blocks = $('.block');

      var images = [
        'misunderstanding.png',
        '4wd.png',
        'bobupanddown.png',
        '4iv.png',
        'bcinu.png'
      ];

      function randomFrom(collection) {
        var index = Math.floor( Math.random() * collection.length );
        return collection[index];
      }

      function randomImage() {
        return randomFrom(images);
      }

      function removeImage(img) {
        var index = images.indexOf(img);
        if (index != -1)
          images.splice(index, 1);
      }

      function updateImage() {
        var newImage = randomImage();
        removeImage(newImage);
        $('.wrapper').css('background-image', 'url(' + newImage + ')');
      }

      function resetBoard() {
        $('.block').addClass('dark').css('opacity', 1);
        updateImage();
      }

      updateImage();

      $('.go').click(function () {
        
        // find a random one!
        var index = Math.floor( Math.random() * $blocks.length );
        $blocks.eq(index).animate({ opacity: 0 }, 2000, function () {
          $(this).removeClass('dark');
          $blocks = $('.dark');
        });
      });

      $('.reset').click(resetBoard);
    </script>
  </body>
</html>











