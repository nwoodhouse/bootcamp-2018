<script>
    window.routes = {
        buildUrl: function (routeString, data) {
            return routeString.replace('-id-', data.id);
        },
        api: {
            dogs: {
                index: '{{ route('dogs.index') }}',
                show: '{{ route('dogs.show', '-id-') }}'
            },

        }
    };
</script>