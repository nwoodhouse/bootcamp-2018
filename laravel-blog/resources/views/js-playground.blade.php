<html>

	<body>
		<h1 id="main-heading">
			Primes Calculator
		</h1>

		<form action="#">
			<input type="number" name="candidate"> : Enter a number
			<button>Go!</button>
		</form>

		<h3>
			The number <span id="result">RESULT</span> prime
		</h3>
	</body>


	<script>
		
		// read the DOM
		var heading = document.getElementById('main-heading');
		var headingText = heading.innerText;

		// or ... (note - the following returns a bunch of 
		// things, so we will just look at the first item!)
		var input = document.getElementsByTagName('input')[0];
		var inputValue = input.value;
		
		// or... (note - the argument here is the same as a 
		// CSS selection)
		var resultSpan = document.querySelector('#result');

		// write to the DOM!
		heading.innerText = 'Updated!!';

		// note - for inputs, look for their 'value property'
		input.value = 123;


		// bind to the form's submit method
		document.querySelector('form').onsubmit = function (e) {

			// prevent the navigation!
			e.preventDefault();

			// ...???
		};

	</script>
</html>



