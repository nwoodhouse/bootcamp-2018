<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->truncate();

        
        for($i=0; $i<100; $i++)
        {
        	User::create([
	        	'email' => "user$i@calibrae.com",
	        	'name' => "User $i",
	        	'password' => Hash::make('password123'),
	        	'dob' => '1920-01-01'
	        ]);
        }
    }
}
