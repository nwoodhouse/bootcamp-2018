<?php

use App\Dog;
use Illuminate\Database\Seeder;

class DogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dog::create([
        	'name' => 'Bo',
			'img' => 'https://i.amz.mshcdn.com/9xDFv-klq_nrHPBvVRZ3DCzL_oE=/950x534/2015%2F10%2F09%2Fc9%2Fboobama.29ff8.jpg',
			'url' => 'https://en.wikipedia.org/wiki/Bo_(dog)'
		]);
    }
}
