<?php

use App\Post;
use App\User;
use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        DB::table('posts')->truncate();

        // create some posts for each user
        foreach(User::all() as $user)
        {
        	$numPosts = rand(0, 100);
        	for($i=0; $i<$numPosts; $i++)
        	{
        		$user->posts()->create([
        			'title' => 'Title ' . $i,
        			'text' => 'This is post number: ' . $i,
        		]);
        	}
        }
    }
}







