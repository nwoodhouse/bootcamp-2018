<?php

use Illuminate\Database\Seeder;
use App\Car;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Car::truncate();

        // Create some cars to play with!!
    	$lambo = Car::create([
    		'title' => 'Lamborghini Countach',
    		'description' => 'This design language was originally embodied and introduced to the public in 1970 as the Lancia Stratos Zero concept car. The first showing of the Countach prototype was at the 1971 Geneva Motor Show, as the Lamborghini LP500 concept car. ',
    		'engine' => '5.2 Litre Petrol'
    	]);

    	$tesla = Car::create([
    		'title' => 'Tesla model X',
    		'description' => 'Model X combines the space and functionality of a sport utility vehicle and the uncompromised performance of a Tesla.',
    		'engine' => '1.21 Giga Watts'
    	]);
    }
}
