<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// CRUD - all operations in one go!!
Route::resource('dogs', 'DogController')->middleware('auth');

// Create / store
//Route::post('dogs', 'DogController@store')->name('dogs.store');

// Read all
//Route::get('dogs', 'DogController@index')->name('dogs.index');

// Read specific dog
//Route::get('dogs/{dog}', 'DogController@show')->name('dogs.show');

// Delete
//Route::delete('dogs/{dog}', 'DogController@destroy')->name('dogs.destroy');
