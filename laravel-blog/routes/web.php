<?php

Auth::routes();

Route::get('tinker', function () {

	return view('js-playground');
	
});

// Wire up the routes for the cars
Route::resource('cars', 'CarController');

Route::get('dogs', 'DogController@indexHtml')->name('dogs.indexHtml');








// wire up the CRUD resource operations in one go
Route::resource('posts', 'PostController');










// Expose a calculator function! Note - the operator as a route param
Route::get('calculator/{operation}', 'PostController@calc');

Route::get('game', function () {
	$userName = 'Nathan';
	return view('game', compact('userName'));
});






Route::get('users', 'UserController@index')->name('users.index');

Route::get('users/{user}/{slug?}', 'UserController@show')->name('users.show');
















Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
