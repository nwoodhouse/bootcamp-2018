<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    // dynamic attribute
    public function getUrlAttribute()
    {
        return route('users.show', [$this->id, str_slug($this->name) ]);
    }

    // additional dynamic (get[SOMETHING]Attribute) things to be 
    // added to JSON representation
    protected $appends = [
        'url'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'dob',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function posts() 
    {
        return $this->hasMany(Post::class);
    }

}





