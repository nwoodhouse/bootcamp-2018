<?php

namespace App\Policies;

use App\User;
use App\Dog;
use Illuminate\Auth\Access\HandlesAuthorization;

class DogPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the dog.
     *
     * @param  \App\User  $user
     * @param  \App\Dog  $dog
     * @return mixed
     */
    public function view(User $user, Dog $dog)
    {
		return $dog->ownedBy($user);
    }

    /**
     * Determine whether the user can create dogs.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the dog.
     *
     * @param  \App\User  $user
     * @param  \App\Dog  $dog
     * @return mixed
     */
    public function update(User $user, Dog $dog)
    {
        //
    }

    /**
     * Determine whether the user can delete the dog.
     *
     * @param  \App\User  $user
     * @param  \App\Dog  $dog
     * @return mixed
     */
    public function delete(User $user, Dog $dog)
    {
        //
    }

    /**
     * Determine whether the user can restore the dog.
     *
     * @param  \App\User  $user
     * @param  \App\Dog  $dog
     * @return mixed
     */
    public function restore(User $user, Dog $dog)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the dog.
     *
     * @param  \App\User  $user
     * @param  \App\Dog  $dog
     * @return mixed
     */
    public function forceDelete(User $user, Dog $dog)
    {
        //
    }
}
