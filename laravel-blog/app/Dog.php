<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dog extends Model
{
    protected $fillable = [
    	'name',
		'url',
		'img',
		'owner_id'
	];

    protected $appends = [
    	'apiUrl'
	];

    public function getApiUrlAttribute() {
    	return route('dogs.show', $this->id);
	}

	public function owner() {
    	return $this->belongsTo(User::class);
	}

	public function ownedBy(User $user) {
    	return $user->id == $this->owner_id;
	}
}
