<?php

namespace App\Http\Controllers;

use App\Dog;
use Illuminate\Http\Request;

class DogController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth:api', [
			'only' => [
				'show',
				'destroy'
			]
		]);

		$this->middleware('can:view,dog', [
			'only' => [
				'show'
			]
		]);
	}

	/**
	 * Responsible for serving up the landing page for the Dog SPA
	 * which will bootstrap the client-side JS that will consume
	 * the Dogs API so wonderfully implemented in this class
	 */
	public function indexHtml()
	{
		return view('dogs.index');
	}


	/**
	 * API enables sorting via optional order & direction params, E.g.:
	 *
	 * /dogs?order=name
	 * /dogs?order=name&direction=asc
	 *
	 * @return Dog[]|\Illuminate\Database\Eloquent\Collection
	 */
    public function index(Request $req)
	{
		$query = Dog::query();

		$orderProperty = $req->input('order', 'name');
		$direction = $req->input('direction', 'asc');

		$query->orderBy($orderProperty, $direction);

		return $query->get(['id', 'name']);
	}

	public function store(Request $req)
	{

		$params = $req->only('name', 'url', 'img');
		return Dog::create($params);
	}

	public function show(Request $req, Dog $dog)
	{
		return $dog;
	}

	public function destroy(Dog $dog)
	{
		$dog->delete();
		return $dog;
	}
}
