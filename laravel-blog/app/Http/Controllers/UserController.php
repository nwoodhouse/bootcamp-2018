<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

	public function index()
	{
		$users = User::orderBy('name')->get();

		// show them!
		return view('users.index', compact('users'));
	}

	public function show(User $user)
	{
		return view('users.show', compact('user'));
	}

    public function showSignin()
    {
    	return 'This is the signin form!';
    }

    public function doSignin()
    {
    	return 'Blip blip blop - processing the signin request....';
    }
}
