<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalcController extends Controller
{
	/**
	 * @param $x int
	 * @param $y int
	 * @return int
	 */
    public function add($x, $y)
	{
		return $x + $y;
	}
}
