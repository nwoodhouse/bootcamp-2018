<?php

class OddBagBad
{
	private $min, $max, $bag;

	public function __construct($min, $max)
	{
		$this->min = $min;
		$this->max = $max;
		$this->bag = [];
	}

	public function getBag()
	{

		for($i=$this->min; $i < $this->max; $i++)
		{
			if ($i % 2)
				$this->bag[] = $i;
		}

		return $this->bag;
	}
}

$obb = new OddBagBad(0, 100);
echo array_sum($obb->getBag());

echo '<h1>Wahoo</h1>';

// notice how calling the method multiple times yeilds different results each
// time - this behaviour is counter-intuitive and surprising - avoid!
echo array_sum($obb->getBag());




